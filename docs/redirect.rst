PhyDLL
======
Getting started

.. toctree::
    :maxdepth: 2

    ./phydll/install
    ./phydll/run
    ./phydll/acknowledgements


Tutorials
=========
Guide to couple your physical solver to neural network inferences.

.. toctree::
    :maxdepth: 2

    ./tutorials/tutorial
    ./tutorials/ds_nc


Fortran API
===========
Guide to understand Fortran API

.. toctree::
    :maxdepth: 2

    ./tocont/f_api

Python API
==========
Guide to understand Python API

.. toctree::
    :maxdepth: 2

    api/phydll
