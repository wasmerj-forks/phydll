
# Acknowledgements
The development leading to these results has received funding from the European
Union’s Horizon 2020 – Research and Innovation Framework Programme
H2020–INFRAEDI–2019–1 under grant agreement no. 951733.

![CoE_RAISE](../images/Logo_CoE_RAISE.jpg)
<p style="text-align: center;">[https://www.coe-raise.eu/](https://www.coe-raise.eu/)</p>
