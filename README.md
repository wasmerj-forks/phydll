**PhyDLL** (fidɛl) (**Phy**sics **D**eep **L**earning coup**L**er) is an open-source library to couple massively parallel physical solvers to distributed deep learning inferences.

The documentation is available in [phydll.readthedocs.io](https://phydll.readthedocs.io)

![PhyDLL](docs/images/phydll_0.png)

